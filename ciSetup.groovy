gitBaseUrl = "https://bitbucket.org/spaceinjava";
gitBranch = "develop";
gitCredentials = "git_credentials";
gitMSRepos = ["jewel-easy-commons","jewel-easy-config", "jewel-easy", "jewel-easy-gateway", "jewel-easy-tenant", "jewel-easy-product", "jewel-easy-report", "jewel-easy-journal"];
gitUIRepos = ["je-frontend"];


def createDeployJobs(repo_name) {

    def job_name = repo_name + "-deploy-job";
    def git_url = gitBaseUrl + "/" + repo_name;
    
    freeStyleJob(job_name) {
	logRotator(-1, 10)
	configure {
	    project ->
	    project / publishers << 'jenkins.plugins.slack.SlackNotifier' {
		notifyFailure(true)
		notifySuccess(true)
		notificationMessage('SummaryOnly')
		notifyRepeatedFailure(true)
		commitInfoChoice("AUTHORS_AND_TITLES")
	    }
	}	
  	parameters {
	    nodeParam('run_on') {
		description('Runs builds on master node')
		defaultNodes(['master'])
		allowedNodes(['master'])
		trigger('multiSelectionDisallowed')
		eligibility('IgnoreOfflineNodeEligibility')
	    }
	}
	scm {
	    git {
		remote {
		    url(git_url)
		    branch(gitBranch)
		    credentials(gitCredentials)
        	}
	    }	
	}
	triggers { 
	    scm("* * * * *")
	}
	steps {
            shell("docker build . -t " + repo_name)
	}
    }
}
def createUIBuildJobs(repo_name) {

    def job_name = repo_name + "-build-job";
    def git_url = gitBaseUrl + "/" + repo_name;
    
    freeStyleJob(job_name) {
	logRotator(-1, 10)
	configure {
	    project ->
	    project / publishers << 'jenkins.plugins.slack.SlackNotifier' {
		notifyFailure(true)
		notifySuccess(true)
		notificationMessage('SummaryOnly')
		notifyRepeatedFailure(true)
		commitInfoChoice("AUTHORS_AND_TITLES")
	    }
	}	
  	parameters {
	    nodeParam('run_on') {
		description('Runs builds on master node')
		defaultNodes(['master'])
		allowedNodes(['master'])
		trigger('multiSelectionDisallowed')
		eligibility('IgnoreOfflineNodeEligibility')
	    }
	}
	scm {
	    git {
		remote {
		    url(git_url)
		    branch(gitBranch)
		    credentials(gitCredentials)
        	}
	    }	
	}
	triggers { 
	    scm("* * * * *")
	}
	steps {
            shell("docker build . -t " + repo_name)
	}
    }
}

def createMSBuildJobs(repo_name) {
    
    def job_name = repo_name + "-build-job";
    def git_url = gitBaseUrl + "/" + repo_name;
    
    mavenJob(job_name) {
	wrappers {
	    preBuildCleanup()
    	}
	configure {
	    project ->
	    project / publishers << 'jenkins.plugins.slack.SlackNotifier' {
		notifyFailure(true)
		notifySuccess(true)
		notificationMessage('SummaryOnly')
		notifyRepeatedFailure(true)
		commitInfoChoice("AUTHORS_AND_TITLES")
		//teamDomain("spaceinje")
	    }
	}
  	parameters {
	    nodeParam('run_on') {
		description('Runs builds on master node')
		defaultNodes(['master'])
		allowedNodes(['master'])
		trigger('multiSelectionDisallowed')
		eligibility('IgnoreOfflineNodeEligibility')
	    }
	}
	logRotator(-1, 10)
	scm {
	    git {
		remote {
		    url(git_url)
		    branch(gitBranch)
		    credentials(gitCredentials)
        	}
	    }	
	}
	triggers { 
	    scm("* * * * *")
	}
	goals('clean package -Dmaven.test.skip=true')
    }
}

for(repo=0;repo<gitMSRepos.size();repo++) {
    
    createMSBuildJobs(gitMSRepos[repo])
	createDeployJobs(gitMSRepos[repo])
}
for(repo=0;repo<gitUIRepos.size();repo++) {
    
    createUIBuildJobs(gitUIRepos[repo])
}